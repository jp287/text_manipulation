def read_txt(infile):
    """read text file

    :param infile: input txt file
    :return: readlines
    """
    with open(infile, 'r') as f:
    	a = f.readline()
    	b = f.readlines()

    c = []
    for line in b:
        c.append(line.rstrip('\n'))
    
    return c

#c = read_txt('newexample _text.txt')

def count_men():
    c = read_txt('newexample _text.txt')
    
    count = 0

    for line in c:
    	linewords = line.split()
    	for words in linewords:
    		if(words == 'men'):
    		    count+=1

    return count

a = count_men()
print(a)
  